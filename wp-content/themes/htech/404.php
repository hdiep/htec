<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header(); ?>
		<div id="content" class="site-content" role="main">
				<div class="page-content">
					<h2>Trang bạn tìm không tồn tại</h2>
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->
<?php get_footer(); ?>