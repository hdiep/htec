<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 3/4/14
 * Time: 10:10 AM
 * Trang hien thi ket qua search cua Ultimate WP Query Search Filter
 */
?>
<?php get_header() ?>
    <div class="show-content-page">
        <div class="span8">
            <ul class="show-content">
                <h3 class="font-h2">
                    <?php if(isset($_GET['lang'])){
                        echo 'Search results:';
                    } else{

                        echo 'Kết quả Tìm Kiếm:';
                    }?>
                    </h3>
                <div class="link"></div>
                <?php if (have_posts()) : ?>
                 <?php  while ( have_posts() ) : the_post();?>
                        <li class="box-title items">
                            <div class="title-search"> <a href="<?php the_permalink()?>"> <?php echo the_title()?></a></div>
                            <div class="content">
                                <a href="<?php echo the_permalink()?>"> <?php echo the_post_thumbnail()?></a>
                                <span class="details"> <?php echo $content= wp_trim_words(get_the_content(),68,'...' ) ;?></span>
                            </div>
                        </li>
                    <?php endwhile ?>
                <?php endif ?>
            </ul>
        </div>
        <div class="span3">
            <?php include('top-product.php'); ?>
        </div>
    </div>

<?php get_footer() ?>