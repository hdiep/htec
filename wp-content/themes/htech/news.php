
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#slider2").tinycarousel({
            axis   : "y",
            interval: true,
            animationTime: 7000 ,intervalTime: 9000
        });

    });

</script>
<div class="news">
    <h2 class="font-h2">
        <?php if (isset($_GET['lang'])) {
            echo 'NEWS';
        } else {
            echo ' TIN TỨC NỔI BẬT';
        }?>
    </h2>

    <div id="slider2">
        <div class="viewport">
            <ul class="panel-news overview">
                <?php
                $arg = array('category_name' => 'news', 'showposts' => 2, 'orderby' => 'rand'
                );
                $query = new WP_Query($arg);
                if ($query->have_posts())
                    while ($query->have_posts()):
                        $query->the_post();
                        ?>
                        <li>

                            <span> <a class="a-new" href="<?php the_permalink(); ?>" title="<?php echo the_title()?>"><?php echo $title=wp_trim_words(get_the_title(),10,'..');?></a></span>
                            <?php the_post_thumbnail() ?>

                            <p class="short-content" title="<?php echo the_title()?>">
                                <?php echo $content = wp_trim_words(get_the_excerpt(), 18, '<a href="' . get_permalink() . '"> ... Xem tin</a>'); ?>
                            </p>
                        </li>

                    <?php endwhile ?>
            </ul>
        </div>
    </div>
</div>