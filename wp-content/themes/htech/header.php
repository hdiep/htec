<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 4/2/14
 * Time: 5:03 PM
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = 0">
    <title><?php wp_title('', true, 'right'); ?></title>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen"/>
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-responsive.css" type="text/css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="container">
    <div class="navbar">
        <h1><a href="<?php echo esc_url(home_url('/')); ?>" title="High technical">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="logo"/>
            </a>
        </h1>
        <div class="logan"><b style="color: #E00B2E">CÔNG TY TNHH </b> PHÁT TRIỂN CÔNG NGHỆ VÀ THIẾT BỊ CÔNG NGHỆ CAO – HTEC <br>
            <span
                style="font-size: 16px;text-transform: lowercase;font-family: arial, helvetica, sans-serif"></span>
        </div>
        <?php qtrans_generateLanguageSelectCode('image'); ?>
    </div>
    <div class="navbar-collapse">
        <?php wp_nav_menu(array('menu' => ' menu-main')); ?>
        <form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
            <input type="search" class="search-field" placeholder=" <?php if (isset($_GET['lang'])) {
                echo 'Search...';
            } else {
                echo ' Tìm kiếm...';
            }?>"  name="s" title="Search for:"/>
        </form>
    </div>