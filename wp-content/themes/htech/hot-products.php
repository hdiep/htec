<link href="<?php echo get_template_directory_uri(); ?>/css/tinycarousel.css" rel="stylesheet" media="all" title="slide" type="text/css"/>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src=" <?php echo get_template_directory_uri();?>/js/jquery.tinycarousel.js"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#slider1").tinycarousel({ interval: true,animationTime: 7000 ,intervalTime: 7000});
    });
</script>
<h3 class="font-h2">
    <?php if(isset($_GET['lang'])){
        echo 'PRODUCTS';
    } else{
        echo ' SẢN PHẨM TIÊU BIỂU';
    }?>
</h3>
<div id="slider1">
    <div class="viewport">
        <ul class="overview">
            <?php
            $arg=array('category_name'=>'products','showposts' => 5,'orderby'=>'rand');
            $query=new WP_Query($arg);
            $count=$query;
            if($query->have_posts())
            while($query->have_posts()): $query->the_post();?>
            <li>
                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail()?></a>
                <span class="atl"> <?php echo the_title();?></span>
            </li>
            <?php  endwhile?>
        </ul>
    </div>
</div>