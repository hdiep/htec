<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title">
           <?php if(isset($_GET['lang'])){
            echo 'PRODUCTS';
            } else{

            echo 'SẢN PHẨM';
            }?>

        </div>
    </div>
    <ul class="panel-body panel-news">

        <?php
        global $post;
        $arr_pro=array('category_name'=> 'products','orderby'=>'rand','numberposts'=>'5');
        $result_pro=get_posts($arr_pro);
        foreach($result_pro as $post):setup_postdata($post);
            ?>
            <li>
                <span> <a href="<?php the_permalink();?>"><?php the_title();?></a></span>
                <?php the_post_thumbnail();?>
                <p class="short-content">
                    <?php echo $content= wp_trim_words(get_the_content(),28,'' ) ;?>
                </p>
            </li>
        <?php endforeach ?>

    </ul>
</div>