<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 4/9/14
 * Time: 2:35 PM
 */
get_header();?>
<?php if (in_category('products') || post_is_in_descendant_category(2)) { ?>
        <?php include('single-products.php'); ?><!--Hien thi tragn chi tiet sp!-->
<?php } else { ?>
    <div class="show-content-page"><!--Hien thi tragn chi tiet category khac-->
        <div class="span8">
            <div class="show-content">
                <div class="row"> <?php echo meteor_slideshow("Slides", ""); ?></div>
                <h3 class="font-h2"><?php the_category(','); ?></h3>

                <div class="link"></div>
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php setPostViews(get_the_ID()); ?>
                        <div class="box-title">
                            <h4> <?php echo the_title() ?></h4>

                            <div class="infor">
                                <i> - Danh mục:  <?php the_category(','); ?>
                                    - Số lượng xem: <?php echo getPostViews(get_the_ID()); ?>
                                    - Ngày đăng : <?php echo the_date('d/m/Y'); ?>
                                </i>
                            </div>

                            <div class="hrrr"></div>
                            <!---hien thi bai viet lien quan-->
                            <div class="relatived-post">
                                <?php
                                //for use in the loop, list 5 post titles related to first tag on current post
                                global $post;
                                $current_post = $post->ID;
                                $categories = get_the_category();
                                foreach ($categories as $category) :
                                    ?>
                                    <div class="my-recent-posts">
                                        <h5>BÀI VIẾT LIÊN QUAN:</h5>
                                        <ul class="related-post">
                                            <?php
                                            $posts = get_posts('showposts=8&offset=0&orderby=rand&category=' . $category->term_id . '&exclude=' . $current_post);
                                            foreach ($posts as $post) :
                                                ?>
                                                <li><a href="<?php the_permalink(); ?>"
                                                       title="Xem"><?php the_title(); ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <!---end hien thi bai viet lien quan--->
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="span3">
            <?php include('top-news.php'); ?>
            <?php include('surpport-online.php'); ?>
            <?php include('top-video.php'); ?>
        </div>
    </div>
<?php } ?>
<?php get_footer() ?>