<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/14/14
 * Time: 4:16 PM
 */
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

<link href="http://xechuyendung.net.vn/tpl/css/jquery.fancybox.css" rel="stylesheet" type="text/css">

<script src="http://xechuyendung.net.vn/tpl/js/jquery.elevateZoom-2.5.5.min.js" type="text/javascript"></script>
<script src="http://xechuyendung.net.vn/tpl/js/jquery.fancybox.pack.js" type="text/javascript"></script>


<script>
    $(document).ready(function () {
        $(".fancybox").fancybox({
            openSpeed: 600,
            closeSpeed: 600,
            openMethod: 'zoomIn',
            closeMethod: 'zoomOut',
            closeBttn: true,
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(80, 80, 80, 0.8)'
                    }
                },
                title: {
                    type: 'over'
                }
            },
            padding: 0

        });
    });
</script>
<div style="width: 500px">
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <?php $images = get_multiple_image(get_the_ID()); ?>
            <?php if (get_multiple_image(get_the_ID())) { ?>
                <div id="main-image" style="margin-bottom: 5px;">
                  <img src="<?php echo $images['0']; ?>" id="img_main" width="100%" data-zoom-image="<?php echo $images['0']; ?>"/>
                </div>
                <div class="thumbgallery" id="thumb-gal">
                    <?php foreach ($images as $image) : ?>
                    <a href="#" data-image="<?php echo $image; ?>" data-zoom-image="<?php echo $image; ?>">
                        <img src="<?php echo $image; ?>" height="80px;"/>
                    </a>
                    <?php endforeach ?>
                </div>

            <?php } ?>
        <?php endwhile ?>
    <?php endif ?>
    <script>
        $("#img_main").elevateZoom({
            gallery: 'thumb-gal',
            cursor: 'pointer',
            galleryActiveClass: 'active',
            zoomWindowFadeIn: 900,
            zoomWindowFadeOut: 900,
            lensFadeIn: 900,
            lensFadeOut: 900,
            zoomWindowOffetx: 10,
            borderSize: 1,
            borderColor: '#555'
        });

        $("#img_main").bind("click", function (e) {
            var ez = $('#img_main').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });
    </script>
</div>