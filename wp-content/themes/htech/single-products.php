<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/13/14
 * Time: 10:28 AM
 */
?>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.elevateZoom-2.5.5.min.js" type="text/javascript"></script>
<link href="<?php echo get_template_directory_uri()?>/css/jquery.fancybox.css" rel="stylesheet" type="text/css">
<script src="<?php echo get_template_directory_uri()?>/js/main.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
    $(function () {
        // Stick the #nav to the top of the window
        var nav = $('#subnav');
        var navHomeY = nav.offset().top;
        var isFixed = false;
        var $w = $(window);
        $w.scroll(function () {
            var scrollTop = $w.scrollTop();
            var shouldBeFixed = scrollTop > navHomeY;
            if (shouldBeFixed && !isFixed) {
                nav.css({
                    position: 'fixed',
                    top: 0,
                    left: nav.offset().left,
                    width: nav.width(),
                    'border-bottom': '3px solid #33b5e5',
                    'box-shadow': '0px 2px 4px #c4c4c4',
                    '-moz-box-shadow': '0px 2px 4px #c4c4c4',
                    '-webkit-box-shadow': '0px 2px 4px #c4c4c4',
                    '-ms-box-shadow': '0px 2px 4px #c4c4c4'
                });
                isFixed = true;
            }
            else if (!shouldBeFixed && isFixed) {
                nav.css({
                    position: 'static',
                    'border-bottom': 'none',
                    'box-shadow': 'none',
                    '-moz-box-shadow': 'none',
                    '-webkit-box-shadow': 'none',
                    '-ms-box-shadow': 'none'
                });
                isFixed = false;
            }
        });
    });

    <!-- Fancybox -->
    $(document).ready(function () {
        $(".fancybox").fancybox({
            openSpeed: 600,
            closeSpeed: 600,
            openMethod: 'zoomIn',
            closeMethod: 'zoomOut',
            closeBttn: true,
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(80, 80, 80, 0.8)'
                    }
                },
                title: {
                    type: 'over'
                }
            },
            padding: 0

        });
    });
</script>


<div class="details-cate-products product-detail">
    <div class="product-menu" id="subnav">
        <div class="aux">
            <ul class="menu">
                <li><a id="mnuGeneralView" href="#genview">Tổng quan</a></li>
                <li><a id="mnuTechSpec" href="#techspecs">Thông số kỹ thuật</a></li>
                <li><a id="mnuImage" href="#imagal">Thư viện</a></li>
            </ul>
        </div>
    </div>
    <div class="wrappers" id="genview">
        <div class="wrapper-slide">
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) :
            the_post(); ?>
            <?php $images = get_multiple_image(get_the_ID()); ?>
            <?php if (get_multiple_image(get_the_ID())) { ?>
                <div id="main-image" style="margin-bottom: 5px;">
                    <img src="<?php echo $images['0']; ?>" id="img_main" width="100%" class="main"
                         data-zoom-image="<?php echo $images['0']; ?>"/>
                </div>
                <ul class="thumbgallery" id="thumb-gal">
                    <?php $i=0; for ($i=0;$i<3;$i++) : ?>
                        <li>
                            <a href="#" data-image="<?php echo $images[$i]; ?>" data-zoom-image="<?php echo $images[$i];?>">
                                <img src="<?php echo $images[$i]; ?>" />
                            </a>
                        </li>
                    <?php endfor ?>
                </ul>
            <?php
            } else {
                echo "Chưa có ảnh ";
            } ?>
        </div>
        <div class="details">
            <h2 class="h2"><?php echo the_title(); ?></h2>
            <h4 class="h4">Tổng quan:</h4>
            <?php echo the_field('tongquan'); ?>
            <h4 class="h4">Thông số chung:</h4>
            <?php echo the_field('texteare'); ?>
                    <?php
                    $jsps_networks = array( 'twitter', 'facebook', 'google', 'mail' );

                    /* set variable */
                    $my_buttons = get_juiz_sps($jsps_networks, 1); // 1 to show counters

                    /* more later, display buttons */
                    echo $my_buttons;?>
        </div>
    </div>

    <div class="content" id="techspecs">
        <h4 class="h4">Thông số kỹ thuật:</h4>
        <?php echo the_content(); ?>
    </div>
    <div class="image-gallery" id="imagal">
        <div class="aux" style="margin: 0px 20px; max-width: none;">
            <h2 id="lblGallery" style="text-align: center;color: #fff">Thư viện</h2>

            <h3 id="lblExternalImage" style="text-align: center;">Hình ảnh tổng thể</h3>

            <div class="thumbgallery">
                <?php foreach ($images as $image) : ?>
                <a class="fancybox" rel="gallery01"
                   href="<?php echo $image; ?>"> <img
                        src="<?php echo $image; ?>"/> </a>
               <?php endforeach ?>
            </div>
        </div>
    </div>

    <?php endwhile ?>
    <?php endif ?>

    <script>
        $("#img_main").elevateZoom({
            gallery: 'thumb-gal',
            cursor: 'pointer',
            galleryActiveClass: 'active',
            zoomWindowFadeIn: 900,
            zoomWindowFadeOut: 900,
            lensFadeIn: 900,
            lensFadeOut: 900,
            zoomWindowOffetx: 10,
            borderSize: 1,
            borderColor: '#555'
        });

        $("#img_main").bind("click", function (e) {
            var ez = $('#img_main').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });
    </script>
</div>

