<?php get_header() ?>
<div class="show-content-page">
    <div class="form-contact">
        <?php $page_contact = get_page(9);
        $content = apply_filters('the_content', $page_contact->post_content);
        echo $content;  ?>
    </div>
    <div class="form-contact" id="form-contact">
        <?php if(isset($_GET['lang'])){
            echo do_shortcode("[contact-form-7 id='139' title='Form contact']");
        } else{
            echo do_shortcode("[contact-form-7 id='135' title='Liên hệ']");
        }?>
    </div>
</div>
<?php get_footer()?>