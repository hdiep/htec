<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/12/14
 * Time: 11:31 AM
 */
?>
<?php get_header() ?>
    <div class="categories-page">
        <?php if (have_posts()) : ?>
            <?php
            $object = $wp_query->get_queried_object();

            // Get parents of current category
            $parent_id = $object->category_parent;
            $cat_breadcrumbs = '';
            while ($parent_id) {
                $category = get_category($parent_id);
                $cat_breadcrumbs = '<a href="' . get_category_link($category->cat_ID) . '">' . $category->cat_name . '</a> &raquo; ' . $cat_breadcrumbs;
                $parent_id = $category->category_parent;
            }
            $result = $cat_breadcrumbs . $object->cat_name;
            ?>
        <h3 class="title-page"><a href="<?php bloginfo('home'); ?>"><?php _e('Home'); ?></a> &raquo; <?php echo $result; ?></h3>
        <ul class="item-categories">
                <?php while (have_posts()) :
                    the_post(); ?>
                    <li class="post-content clearfix" id="post-<?php the_ID(); ?>">
                        <a href="<?php the_permalink(); ?>" title="Xem"
                           rel="bookmark"><?php echo the_post_thumbnail('medium'); ?></a>

                        <h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title_attribute(); ?></a></h3>

                        <div class="content clearfix"><?php echo the_field('texteare') ?></div>
                        <!--show noi dung ngan-->
                    </li>
                <?php endwhile; ?>
            <?php else: ?>
                <div style="height: 200px;padding: 20px">Chưa có dữ liệu</div>
            </ul>
            <?php endif ?>

    </div>
<?php get_footer() ?>