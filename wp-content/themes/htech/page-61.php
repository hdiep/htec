<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 3/4/14
 * Time: 10:10 AM
 * Trang hien thi ket qua search cua Ultimate WP Query Search Filter
 */
?>
<?php get_header() ?>
    <div class="show-content-page">
        <div class="span8">
            <ul class="show-content">
                <h3 class="font-h2">
                    <?php if(isset($_GET['lang'])){
                        echo 'NEWS';
                    } else{

                            echo ' TIN TỨC';
                    }?>
                </h3>
                <div class="link"></div>
                <?php global $post;
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array(
                    'category_name' => 'news',
                    'posts_per_page' => 6,
                    'numberposts' => '40',
                    'orderby' => 'desc',
                    'paged' => $paged,
                );

                $the_query = new WP_Query($args);
                while ($the_query->have_posts()):$the_query->the_post();?>
                    <li class="box-title items">
                        <div class="title-search"><a href="<?php the_permalink() ?>"> <?php echo the_title() ?></a>
                        </div>
                        <div class="content">
                            <a href="<?php echo the_permalink() ?>"> <?php echo the_post_thumbnail() ?></a>
                            <span
                                class="details"> <?php echo $content = wp_trim_words(get_the_content(), 68, '...'); ?></span>
                        </div>
                    </li>

                <?php endwhile ?>
                <?php $big = 999999999; // need an unlikely integer
                echo paginate_links(array(
                    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                    'format' => '?paged=%#%',
                    'current' => max(1, get_query_var('paged')),
                    'total' => $the_query->max_num_pages));
                ?>
            </ul>
        </div>

        <div class="span3">
            <?php include('top-product.php'); ?>
        </div>
    </div>

<?php get_footer() ?>