<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/12/14
 * Time: 1:20 PM
 */
?>
<?php get_header() ?>
    <script src="<?php echo get_template_directory_uri()?>/js/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/js/main.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri()?>/js/jquery.flexslider.js"></script>
    <script src='<?php echo get_template_directory_uri()?>/js/jquery.min.1.js' type='text/javascript'></script>
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/product.css">
    <script charset="utf-8" type="text/javascript">
        var _rys = jQuery.noConflict();
        _rys("document").ready(function(){
            _rys(window).scroll(function () {
                if (_rys(this).scrollTop() > 170) {
                    _rys('.nav-containers').addClass("f-nav");
                } else {
                    _rys('.nav-containers').removeClass("f-nav");
                }
            });
        });
    </script>
    <div class="categories">
        <div class="product">
            <h3 class="title-page"><a href="<?php bloginfo('home'); ?>"><?php _e('Home'); ?></a> &raquo; Sản Phẩm </h3>
            <div class="aux">
                <nav id="navmenu" class="left-nav nav-containers">
                    <ul>
                        <?php
                        $stt = 0;
                        $subcategories = get_categories('&child_of=2');
                        echo '<ul class="parent-cate">';
                        foreach ($subcategories as $subcategory) {
                            $stt = $stt + 1;
                            ?>
                            <li><a href="#<?php echo $stt ?>"><?php echo $subcategory->name ?></a></li>
                        <?php } ?>
                        <?php $stt++; ?>
                    </ul>
                </nav>
                <div class="content">
                    <?php
                    $sttt = 0;
                    $childCats = get_categories('child_of=2');
                    if (is_array($childCats)) {
                        foreach ($childCats as $child) {
                            $sttt = $sttt + 1;?>
                            <div id="<?php echo $sttt ?>" class="group grpRight <?php if($sttt%2==0){echo "even";}else{echo "old";}?>">
                                <h2><?php echo $child->name; ?></h2>
                                <div class="wrapper">
                                    <div class="slide-content">
                                        <div class="flexslider" id="slide<?php echo $sttt; ?>">
                                            <ul class="slides">
                                                <?php query_posts('cat=' . $child->term_id);
                                                while (have_posts()): the_post();
                                                $do_not_duplicate = $post->ID; ?>
                                                <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;">
                                                <?php echo the_post_thumbnail(array(400,225)); ?>
                                                </li>
                                                <?php endwhile;
                                                wp_reset_query(); ?>
                                            </ul>
                                        </div>
                                        <script>
                                            $(window).load(function () {
                                                $('#slide<?php echo $sttt;?>').flexslider({
                                                    animation: "fade",
                                                    slideshowSpeed: "4000",
                                                    animationSpeed: "500",
                                                    controlNav: false,
                                                    directionNav: false,
                                                    keyboard: false,
                                                    touch: false,
                                                    pauseOnAction: false,
                                                    randomize: true
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="side-content">
                                        <ul>
                                            <?php query_posts('cat=' . $child->term_id);
                                            while (have_posts()): the_post();
                                                $do_not_duplicate = $post->ID; ?>
                                                <li><a href="<?php echo the_permalink() ?>"><?php echo the_title() ?></a></li>
                                            <?php endwhile;
                                            wp_reset_query(); ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        $sttt++;  }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>