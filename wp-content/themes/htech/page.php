<?php
/**
 * Created by JetBrains PhpStorm.
 * User: diep
 * Date: 4/9/13 4:39 PM
 * File: page.php
 * Support : diep@lms.vn
 */
get_header();
?>
    <div class="show-content-page">
    <div class="span8">
      <div class="show-content">

        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; // end of the loop. ?>
    </div>
    </div>
    <div class="span3">
     <?php include('surpport-online.php');?>
            <?php include('top-video.php');?>
    </div>
</div>
<?php get_footer(); ?>