<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title">VIDEO</div>
    </div>
    <div class="panel-body">
        <?php
        $arg=array('category_name'=>'videos','showposts' => 2,'orderby'=>'rand'
        );
        $query=new WP_Query($arg);
        if($query->have_posts())
        while($query->have_posts()): $query->the_post();
        ?>
        <div class="item-supports"><?php echo the_content();?></div>

        <?php endwhile ?>
    </div>
</div>