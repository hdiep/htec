</div>
<footer>
    <?php  wp_footer()?>
    <div class="container-footer">
    <div class="footer-left">
        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="logo"/>
        <div class=" address2">
            <?php
           // $id=33;
            $post = get_post(33);
            $content = apply_filters('the_content', $post->post_content);
            echo $content;
            ?>
        </div>
        <div class="address">
            Hotline: 0916.80.82.68 (Mr. Huy)  - 0915.11.55.79 (Miss. Thiết)  - 0916.44.84.68 (Miss. Linh)
        </div>
    </div>
    <div class="footer-right">
        <?php echo ctsocial_icons_template(); ?>
    </div>
    </div>
</footer>
</body>
</html>