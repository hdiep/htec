<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 4/9/14
 * Time: 2:35 PM
 */
get_header();
?>
<div class="show-content-page">
    <div class="span8">
        <div class="categorys">
            <ul class="category">
                <h3 class="font-h2">
                    <?php if (isset($_GET['lang'])) {
                        echo 'PRODUCTS';
                    } else {

                        echo ' Sản phẩm .........:';
                    }?>
                </h3>

                <div class="link"></div>
                <?php

                $query = query_posts(array('showposts' => 20, 'orderby' => 'rand', 'category_name' => 'products')
                );

                if (have_posts())
                    foreach ($query as $post) : setup_postdata($post); ?>

                        <li class="item-cate">
                            <?php echo the_post_thumbnail() ?>
                            <div class="title-cate"><a
                                    href="<?php echo the_permalink(); ?>"> <?php echo the_title() ?></a></div>
                        </li>
                    <?php endforeach ?>
            </ul>
        </div>
    </div>
    <div class="span3">
        <?php include('top-news.php'); ?>
        <?php include('surpport-online.php'); ?>
    </div>
</div>
<?php get_footer(); ?>
