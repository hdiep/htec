<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 4/2/14
 * Time: 5:04 PM
 */

add_theme_support('post-thumbnails');

register_nav_menus( array(
    'primary' => __( 'Primary Navigation', 'hightech' ),
) );

if ( ! is_admin() ) {
// Hook in early to modify the menu
// This is before the CSS "selected" classes are calculated
    add_filter( 'wp_get_nav_menu_items', 'display_lasts_ten_posts_for_categories_menu_item', 100, 3 );
}

// Add the ten last posts of af categroy menu item in a sub menu
function display_lasts_ten_posts_for_categories_menu_item( $items, $menu, $args ) {


    $menu_order = count($items); /* Offset menu order */
    $child_items = array();

// Loop through the menu items looking category menu object
    foreach ( $items as $item ) {

        // Test if menu item is a categroy and has no sub-category
        if ( 'category' != $item->object || ('category' == $item->object && get_category_children($item->object_id)) )
            continue;

        // Query the lasts ten category posts
        $category_ten_last_posts = array(
            'numberposts' => 100,
            'cat' => $item->object_id,
            'orderby' => 'date',
            'order' => 'DESC'
        );

        foreach ( get_posts( $category_ten_last_posts ) as $post ) {
            // Add sub menu item
            $post->menu_item_parent = $item->ID;
            $post->post_type = 'nav_menu_item';
            $post->object = 'custom';
            $post->type = 'custom';
            $post->menu_order = ++$menu_order;
            $post->title = $post->post_title;
            $post->url = get_permalink( $post->ID );
            /* add children */
            $child_items[]= $post;
        }
    }
    return array_merge( $items, $child_items );
}
//BEGIN thiet lap so luong truy cap bai viet
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//BEGIN lay so luong truy cap bai viet
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count;
}
//end
//in_category#Testing_if_a_post_is_in_a_descendant_category

if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
    function post_is_in_descendant_category( $cats, $_post = null ) {
        foreach ( (array) $cats as $cat ) {
            // get_term_children() accepts integer ID only
            $descendants = get_term_children( (int) $cat, 'category' );
            if ( $descendants && in_category( $descendants, $_post ) )
                return true;
        }
        return false;
    }
}

?>
