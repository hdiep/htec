// JavaScript Document

function toggleID(sID){
    $(sID).slideToggle(300,'linear');
};

$(document).ready(function() {
    // Show/Hide Navigation Drawer
    $('.nav-launcher').click(function() {
        toggleID('.nav-drawer');
    });
    $('.nav-launcher').mouseover(function() {
        toggleID('.nav-drawer');
    });
    $('.nav-drawer').mouseleave(function() {
        toggleID('.nav-drawer');
    });

    // Scroll to element ID
    $('a[href^="#"]').on('click',function (e) {
        e.preventDefault();
        var target = this.hash,
            $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 800, 'swing', function () {
            window.location.hash = target;
        });
    });

    // Go to top
    var pxShow = 500;//height on which the button will show
    var fadeInTime = 1000;//how slow/fast you want the button to show
    var fadeOutTime = 1000;//how slow/fast you want the button to hide
    var scrollSpeed = 1000;//how slow/fast you want the button to scroll to top. can be a value, 'slow', 'normal' or 'fast'
    jQuery(window).scroll(function(){
        if(jQuery(window).scrollTop() >= pxShow){
            jQuery("#backtotop").fadeIn(fadeInTime);
        }else{
            jQuery("#backtotop").fadeOut(fadeOutTime);
        }
    });
    jQuery('#backtotop a').click(function(){
        jQuery('html, body').animate({scrollTop:0}, scrollSpeed);
        return false;
    });
});