<?php get_header(); ?>
    <div class="row"> <?php  echo meteor_slideshow( "Slides", "" ); ?></div>
    <div class="row content-area">

        <div class="span8">
            <?php include('about-us.php');?>
            <div class="show-products">
                <?php include('hot-products.php');?>
            </div>

        </div>

        <div class="span3">
            <?php include('news.php');?>
        </div>
        <?php include('support-video.php');?>

    </div>
<?php get_footer(); ?>